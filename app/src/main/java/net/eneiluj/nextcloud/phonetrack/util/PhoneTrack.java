package net.eneiluj.nextcloud.phonetrack.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
//import android.preference.PreferenceManager;
import androidx.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatDelegate;

public class PhoneTrack extends Application {
    private static final String DARK_THEME = "darkTheme";

    @Override
    public void onCreate() {
        setAppTheme(getAppTheme(getApplicationContext()));
        super.onCreate();
        SystemLogger.getDb(getApplicationContext());
    }

    public static void setAppTheme(Boolean darkTheme) {
        if (darkTheme) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    public static boolean getAppTheme(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(DARK_THEME, false);
    }
}
