# PhoneTrack-Android Privacy Policy

PhoneTrack-Android does not collect any personal data for its developers or any other entity.
This is a Free/Libre software so you can verify no data is collected
by reading the source code or asking someone to do it.

If Nextcloud account is configured,
sessions are synchronized with
the Nextcloud server of your choice.

If PhoneTrack-Android is configured to send your position to a server (Nextcloud or any capable server),
the privacy policy of this server applies.

* ACCESS\_BACKGROUND\_LOCATION permission is used to get the device position when logjobs are enabled or to display the device position on the map.
* RECEIVE\_BOOT\_COMPLETED permission is used to start PhoneTrack-Android on device boot.
* READ\_CONTACTS, SEND\_SMS and RECEIVE\_SMS permissions are used to catch commands received by SMS and send the answer back. The only SMS that are processed are those targeting PhoneTrack-Android.
* GET\_ACCOUNTS permission is used by Single-Sign-On library to access accounts from Nextcloud Files app.
* WRITE\_EXTERNAL\_STORAGE permission is used to write imported map files and to export GPX files.
